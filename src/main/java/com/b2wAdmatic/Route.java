package com.b2wAdmatic;

import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.athena.AmazonAthena;
import com.amazonaws.services.athena.AmazonAthenaClientBuilder;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("route")
public class Route {

    @GET
    @Path("{brand}/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getAthena(@PathParam("brand") String brand,
                            @PathParam("id") String id,
                            @QueryParam( "year" ) String year,
                            @QueryParam( "month" ) String month,
                            @QueryParam( "day" ) String day) {
        try {
            AmazonAthena client =
                    AmazonAthenaClientBuilder.standard().withRegion(Regions.US_EAST_1)
                                                        .withCredentials(DefaultAWSCredentialsProviderChain
                                                        .getInstance()).build();

            AthenaConnector at = new AthenaConnector(client, "admatic");
            List<AthenaRow> result = null;
            String query = "SELECT products FROM product_info_"+brand.toLowerCase()+"_"+year+"_"+month+"_"+day+
                           " WHERE ids[1] = "+id;
            result = at.query(query);
            return result.toString();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return e.getMessage();
        }

    }
}