package com.b2wAdmatic;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class AthenaRow {

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");

    private Map<String, Object> map;

    public AthenaRow() {
        this.map = new HashMap<String, Object>();
    }

    public void setInt(String key, int value) {
        map.put(key, value);
    }

    public int getInt(String key) {
        return Integer.parseInt(map.get(key).toString());
    }

    public void setArray(String key, String value) { map.put(key, value); }

    public String getArray(String key) { return (String) map.get(key); }

    public void setString(String key, String value) {
        map.put(key, value);
    }

    public String getString(String key) {
        return (String)map.get(key);
    }


    public void setDouble(String key, Double value) {
        map.put(key, value);
    }

    public Double getDouble(String key) {
        return (Double)map.get(key);
    }


    public void setBoolean(String key, Boolean value) {
        map.put(key, value);
    }

    public Boolean getBoolean(String key) {
        return (Boolean)map.get(key);
    }


    public void setLong(String key, Long value) {
        map.put(key, value);
    }

    public Long getLong(String key) {
        return (Long)map.get(key);
    }

    public void setTimestamp(String key, Timestamp value) {
        map.put(key, value);
    }

    public Timestamp getTimestamp(String key) {
        return (Timestamp)map.get(key);
    }


    public void setDate(String key, String value) {
        try {
            map.put(key,  dateFormat.parse(value));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public Date getDate(String key) {
        return (Date)map.get(key);
    }

    public String toString() {
        return map.toString();
    }
}