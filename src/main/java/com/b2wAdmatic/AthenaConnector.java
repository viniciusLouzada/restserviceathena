package com.b2wAdmatic;

import java.util.ArrayList;
import java.util.List;

import com.amazonaws.services.athena.AmazonAthena;
import com.amazonaws.services.athena.model.ColumnInfo;
import com.amazonaws.services.athena.model.GetQueryExecutionRequest;
import com.amazonaws.services.athena.model.GetQueryExecutionResult;
import com.amazonaws.services.athena.model.GetQueryResultsRequest;
import com.amazonaws.services.athena.model.GetQueryResultsResult;
import com.amazonaws.services.athena.model.QueryExecutionContext;
import com.amazonaws.services.athena.model.QueryExecutionState;
import com.amazonaws.services.athena.model.ResultConfiguration;
import com.amazonaws.services.athena.model.Row;
import com.amazonaws.services.athena.model.StartQueryExecutionRequest;
import com.amazonaws.services.athena.model.StartQueryExecutionResult;

public class AthenaConnector {

    private AmazonAthena client;

    private String database;

    public AthenaConnector(AmazonAthena client, String database) {
        this.client = client;
        this.database = database;
    }

    public List<AthenaRow> query(String query) {
        List<AthenaRow> rows = null;

        try {
            String queryExecutionId = submitAthenaQuery(query);
            waitForQueryToComplete(queryExecutionId);
            rows = processResultRows(queryExecutionId);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return rows;
    }

    /**
     * Submits a sample query to Athena and returns the execution ID of the
     * query.
     */
    public String submitAthenaQuery(String query) {
        // The QueryExecutionContext allows us to set the Database.
        QueryExecutionContext queryExecutionContext = new QueryExecutionContext().withDatabase(database);

        // The result configuration specifies where the results of the query
        // should go in S3 and encryption options
        ResultConfiguration resultConfiguration = new ResultConfiguration()
                // You can provide encryption options for the output that is written.
                // .withEncryptionConfiguration(encryptionConfiguration)
                .withOutputLocation("s3://admatic-data/product-info/");

        // Create the StartQueryExecutionRequest to send to Athena which will
        // start the query.
        StartQueryExecutionRequest startQueryExecutionRequest = new StartQueryExecutionRequest()
                .withQueryString(query)
                .withQueryExecutionContext(queryExecutionContext)
                .withResultConfiguration(resultConfiguration);

        StartQueryExecutionResult startQueryExecutionResult = client.startQueryExecution(startQueryExecutionRequest);
        return startQueryExecutionResult.getQueryExecutionId();
    }

    /**
     * Wait for an Athena query to complete, fail or to be cancelled. This is
     * done by polling Athena over an interval of time. If a query fails or is
     * cancelled, then it will throw an exception.
     */
    public void waitForQueryToComplete(String queryExecutionId) throws InterruptedException {

        GetQueryExecutionRequest getQueryExecutionRequest = new GetQueryExecutionRequest().withQueryExecutionId(queryExecutionId);

        GetQueryExecutionResult getQueryExecutionResult = null;
        boolean isQueryStillRunning = true;
        while (isQueryStillRunning) {
            getQueryExecutionResult = client.getQueryExecution(getQueryExecutionRequest);
            String queryState = getQueryExecutionResult.getQueryExecution().getStatus().getState();
            if (queryState.equals(QueryExecutionState.FAILED.toString())) {
                throw new RuntimeException(
                        "Query Failed to run with Error Message: " + getQueryExecutionResult.getQueryExecution()
                                .getStatus().getStateChangeReason());
            } else if (queryState.equals(QueryExecutionState.CANCELLED
                    .toString())) {
                throw new RuntimeException("Query was cancelled.");
            } else if (queryState.equals(QueryExecutionState.SUCCEEDED
                    .toString())) {
                isQueryStillRunning = false;
            } else {
                // Sleep an amount of time before retrying again.
                Thread.sleep(60 * 1000);
            }
            System.out.println("Current Status is: " + queryState);
        }
    }

    /**
     * This code calls Athena and retrieves the results of a query. The query
     * must be in a completed state before the results can be retrieved and
     * paginated. The first row of results are the column headers.
     */
    public List<AthenaRow> processResultRows(String queryExecutionId) {
        GetQueryResultsRequest getQueryResultsRequest = new GetQueryResultsRequest()
                // Max Results can be set but if its not set,
                // it will choose the maximum page size
                // As of the writing of this code, the maximum value is 1000
                // .withMaxResults(1000)
                .withQueryExecutionId(queryExecutionId);

        GetQueryResultsResult getQueryResultsResult = client.getQueryResults(getQueryResultsRequest);
        List<ColumnInfo> columnInfoList = getQueryResultsResult.getResultSet().getResultSetMetadata().getColumnInfo();

        List<AthenaRow> rows = null;

        while (true) {
            List<Row> results = getQueryResultsResult.getResultSet().getRows();
            int i = 0;
            for (Row row : results) {
                if ((i++) == 0) continue;//The first row of the first page holds the column names.
                // Process the row
                AthenaRow ar = processRow(row, columnInfoList);
                if (rows == null && ar != null) {
                    rows = new ArrayList<AthenaRow>();
                }
                rows.add(ar);
            }
            // If nextToken is null, there are no more pages to read. Break out of the loop.
            if (getQueryResultsResult.getNextToken() == null) {
                break;
            }
            getQueryResultsResult = client.getQueryResults(getQueryResultsRequest.withNextToken(getQueryResultsResult.getNextToken()));
        }

        return rows;
    }

    public AthenaRow processRow(Row row, List<ColumnInfo> columnInfoList) {
        AthenaRow ar = new AthenaRow();

        for (int i = 0; i < columnInfoList.size(); ++i) {

            switch (columnInfoList.get(i).getType()) {
                case "varchar":
                    ar.setString(columnInfoList.get(i).getName(), row.getData().get(i).getVarCharValue());
                    break;
                case "string":
                    ar.setString(columnInfoList.get(i).getName(), row.getData().get(i).getVarCharValue());
                    break;
                case "tinyint":
                    ar.setInt(columnInfoList.get(i).getName(), Integer.parseInt(row.getData().get(i).getVarCharValue()));
                    break;
                case "smallint":
                    ar.setInt(columnInfoList.get(i).getName(), Integer.parseInt(row.getData().get(i).getVarCharValue()));
                    break;
                case "integer":
                    ar.setInt(columnInfoList.get(i).getName(), Integer.parseInt(row.getData().get(i).getVarCharValue()));
                    break;
                case "bigint":
                    ar.setInt(columnInfoList.get(i).getName(), Integer.parseInt(row.getData().get(i).getVarCharValue()));
                    break;
                case "double":
                    ar.setDouble(columnInfoList.get(i).getName(), Double.parseDouble(row.getData().get(i).getVarCharValue()));
                    break;
                case "float":
                    ar.setDouble(columnInfoList.get(i).getName(), Double.parseDouble(row.getData().get(i).getVarCharValue()));
                    break;
                case "boolean":
                    ar.setBoolean(columnInfoList.get(i).getName(), Boolean.parseBoolean(row.getData().get(i).getVarCharValue()));
                    break;
                case "timestamp":
                    ar.setDate(columnInfoList.get(i).getName(), row.getData().get(i).getVarCharValue());
                    break;
                case "array":
                    ar.setArray(columnInfoList.get(i).getName(), row.getData().get(i).getVarCharValue());
                    break;
                default:
                    throw new RuntimeException("Unexpected Type is not expected" + columnInfoList.get(i).getType());
            }
        }

        return ar;
    }
}
